$(function(){
    jQuery("#checkbox4").change(function(){
        jQuery(".budget").toggleClass("on");
        $('.budgetInput').prop('readonly', function(i, v) { return !v; });
        jQuery(".budgetInput").toggleClass("on");        
    });
    
    jQuery(".btnViewMore-ByModel").click(function(){
        jQuery(".boxViewMore-ByModel").toggleClass("show");
        jQuery(".icon-plus").toggleClass("active");
    });
    jQuery(".btnViewComments").click(function(){
        jQuery(".boxViewComments").toggleClass("show");
        jQuery(".icon-plus").toggleClass("active");
    });
    jQuery(".btnBudgetColumn.1").click(function(){
        jQuery(".budget.1").toggleClass("on");
        jQuery(".budgetInput.1.column").toggleClass("show");
        $('.budgetInput.1').prop('readonly', function(i, v) { return !v; });
        $( ".budgetInput.1" ).click(function() {
		  jQuery(this).toggleClass("on");
		});
    });
    jQuery(".btnBudgetColumn.2").click(function(){
        jQuery(".budget.2").toggleClass("on");
        jQuery(".budgetInput.2.column").toggleClass("show");
        $('.budgetInput.2').prop('readonly', function(i, v) { return !v; });
        $( ".budgetInput.2" ).click(function() {
		  jQuery(this).toggleClass("on");
		});
    });
    jQuery(".btnBudgetColumn.3").click(function(){
        jQuery(".budget.3").toggleClass("on");
        jQuery(".budgetInput.3.column").toggleClass("show");
        $('.budgetInput.3').prop('readonly', function(i, v) { return !v; });
        $( ".budgetInput.3" ).click(function() {
		  jQuery(this).toggleClass("on");
		});
    });
    jQuery(".btnBudgetColumn.4").click(function(){
        jQuery(".budget.4").toggleClass("on");
        jQuery(".budgetInput.4.column").toggleClass("show");
        $('.budgetInput.4').prop('readonly', function(i, v) { return !v; });
        $( ".budgetInput.4" ).click(function() {
		  jQuery(this).toggleClass("on");
		});
    });
});

